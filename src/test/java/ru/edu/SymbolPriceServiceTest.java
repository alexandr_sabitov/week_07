package ru.edu;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;

import java.time.Instant;


import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SymbolPriceServiceTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SymbolPriceServiceTest.class);

    public static final String SYMBOL_NAME = "BTCUSDT";

    private SymbolPriceService service;

    private Symbol mockSymbol1 = mock(Symbol.class);
    private Symbol mockSymbol2 = mock(Symbol.class);


    @Test
    public void getPrice() throws InterruptedException {


        /**
         * примерный тест проверки работы кэша, делаем 2 вызова и смотрим что данные были получены в одно время
         * после ожидания таймаута жизни данных в кэше делаем повторный запрос и проверяем, что данные имеют метку времени,
         * полученную после сброса.
         */
        service = mock(SymbolPriceService.class);


        when(service.getPrice(SYMBOL_NAME)).thenReturn(mockSymbol1);
        when(mockSymbol1.getTimeStamp()).thenReturn(Instant.now());

        Symbol price = service.getPrice(SYMBOL_NAME);
        Symbol priceAgain = service.getPrice(SYMBOL_NAME);
        assertEquals(price.getTimeStamp(), priceAgain.getTimeStamp());


        long timeout = 5000L;
        LOGGER.info("Ждем сброса кэша: {} сек", timeout);
        Thread.sleep(timeout);

        when(service.getPrice(SYMBOL_NAME)).thenReturn(mockSymbol2);
        when(mockSymbol2.getTimeStamp()).thenReturn(Instant.now());

        Symbol priceNew = service.getPrice(SYMBOL_NAME);
        assertTrue(price.getTimeStamp().isBefore(priceNew.getTimeStamp()));
    }
}