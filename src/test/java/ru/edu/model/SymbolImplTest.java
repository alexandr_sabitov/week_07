package ru.edu.model;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;

import static org.junit.Assert.*;

public class SymbolImplTest extends TestCase {

    SymbolImpl symbol = new SymbolImpl();

    @Test
    public void test() {
        String s = "USDBTC";
        symbol = new SymbolImpl(s,new BigDecimal("5"),Instant.now());
        assertNotNull(symbol);
        Assert.assertNotNull(symbol);
        assertEquals(s,symbol.getSymbol());
    }


    @Test
    public void getSymbol() {
        String s = "USDBTC";
        symbol.setSymbol(s);
        assertEquals(s,symbol.getSymbol());
    }

    @Test
    public void getPrice() {
        BigDecimal price = new BigDecimal("300");
        symbol.setPrice(price);
        assertEquals(price,symbol.getPrice());
    }



    @Test
    public void getTimeStamp() {
        Instant instant = Instant.now();
        symbol.setTimestamp(instant);
        assertEquals(instant, symbol.getTimeStamp());
    }

    @Test
    public void testToString() {
        Instant instant = Instant.now();
        symbol = new SymbolImpl("BTC", new BigDecimal(500), instant);
        assertEquals("SymbolImpl{symbol='BTC', price=500, timestamp="+ instant + "}", symbol.toString());
    }

}