package ru.edu.model;

import org.junit.Test;
import ru.edu.SymbolPriceService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class WatcherJobTest {


    @Test
    public void run() throws InterruptedException {
        SymbolPriceService service = new BinanceSymbolPriceService();
        SymbolPriceService cached = new CachedSymbolPriceService(service, 5);

        String[] symbols = {"BTCUSDT", "BTCUSDT", "BTCRUB"};

        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(symbols.length);

        for (String symbol : symbols) {
            executorService.scheduleAtFixedRate(new WatcherJob(symbol, cached), 0, 2, TimeUnit.SECONDS);
        }

        Thread.sleep(15000);
        executorService.shutdown();
        assertTrue(executorService.isShutdown());

    }
}