package ru.edu.model;

import org.junit.Test;
import ru.edu.SymbolPriceService;

import static org.junit.Assert.*;

public class CachedSymbolPriceServiceTest {

    @Test
    public void getPrice() throws InterruptedException {
        SymbolPriceService service = new CachedSymbolPriceService(new BinanceSymbolPriceService(),2);
        Symbol symbol = service.getPrice("BTCRUB");
        assertNotNull(symbol);
        Thread.sleep(6000L);
        Symbol symbol2 = service.getPrice("BTCRUB");
        assertNotNull(symbol2);
        assertTrue(symbol.getTimeStamp().isBefore(symbol2.getTimeStamp()));

    }
}