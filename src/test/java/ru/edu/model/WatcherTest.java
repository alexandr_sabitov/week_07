package ru.edu.model;

import org.junit.Test;
import ru.edu.SymbolPriceService;

import static org.junit.Assert.*;

public class WatcherTest {

    @Test
    public void run() throws InterruptedException {
        SymbolPriceService service = new BinanceSymbolPriceService();
        SymbolPriceService cached = new CachedSymbolPriceService(service, 5);

        String[] symbols = {"BTCUSDT", "BTCUSDT", "BTCRUB"};

        Thread[] threads = new Thread[symbols.length];

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(new Watcher(symbols[i], cached));
            threads[i].start();
        }
        for (Thread thread : threads) {
            assertNotNull(thread);
        }
        Thread.sleep(20_000);

        for (Thread thread : threads) {
            thread.interrupt();
        }
    }
}