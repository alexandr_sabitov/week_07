package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

public class SymbolImpl implements Symbol {

    /**
     * Название котировки.
     */
    private String symbol;

    /**
     * Цена.
     */
    private BigDecimal price;

    /**
     * Время получения данных.
     */
    private Instant timestamp = Instant.now();

    /**
     * Конструктор по умолчанию.
     * Для RestTemplate, который будет с помощью ObjectMapper
     * преобразовывать строку в Java объект
     */
    public SymbolImpl() {
    }

    /**
     * Конструктор.
     *
     * @param sym     - Строка котировки
     * @param p       - цена
     * @param instant - время создания
     */
    public SymbolImpl(final String sym,
                      final BigDecimal p,
                      final Instant instant) {
        this.symbol = sym;
        this.price = p;
        this.timestamp = instant;
    }

    /**
     * Получение  имени котировки.
     *
     * @return symbol
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * Получение цены.
     *
     * @return price
     */
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param decimal set .
     */
    public void setPrice(final BigDecimal decimal) {
        this.price = decimal;
    }

    /**
     * @return Возврат времени получения данных.
     */
    @Override
    public Instant getTimeStamp() {
        return timestamp;
    }

    /**
     * @param timeinstantp set.
     */
    public void setTimestamp(final Instant timeinstantp) {
        this.timestamp = timeinstantp;
    }


    /**
     * @param s Передача символа котировки.
     */
    public void setSymbol(final String s) {
        this.symbol = s;
    }

    /**
     * @return string
     * toString.
     */
    @Override
    public String toString() {
        return "SymbolImpl{"
                + "symbol='" + symbol + '\''
                + ", price=" + price
                + ", timestamp=" + timestamp
                + '}';
    }
}
