package ru.edu.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.edu.SymbolPriceService;



/**
 * Сервис для получения котировок из Binance.
 */
public class BinanceSymbolPriceService implements SymbolPriceService {

    /**
     * Логгер.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SymbolPriceService.class);

    /**
     * Объект RestTemplate.
     */
    private final RestTemplate restTemplate = new RestTemplate();

    /**
     * @param symbolName - строка котировки
     * @return symbol - возврат экземпляра класса Symbol
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные будут
     * обновляться не чаще, чем раз в 10 секунд.
     */
    @Override
    public Symbol getPrice(final String symbolName) {
        ResponseEntity<SymbolImpl> response = restTemplate.getForEntity(
                "https://api.binance.com/api/v3/ticker/price?symbol="
                        + symbolName, SymbolImpl.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            LOGGER.error("Не получили ответ. Код ответа: {}. ",
                    response.getStatusCode());
            return null;
        }
        SymbolImpl symbol = response.getBody();
        LOGGER.debug("Получили ответ для {}: {}", symbolName, symbol);
        return symbol;
    }
}
