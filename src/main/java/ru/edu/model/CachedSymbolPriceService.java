package ru.edu.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.SymbolPriceService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class CachedSymbolPriceService implements SymbolPriceService {

    /**
     * Класс-делегат. Получение стоимости из внешнего источника.
     */
    private final SymbolPriceService delegate;

    /**
     * Кэш.
     */
    private final Map<String, Symbol> cache = Collections
            .synchronizedMap(new HashMap<>());

    /**
     * Logger.
     */
    private final Logger logger = LoggerFactory
            .getLogger(CachedSymbolPriceService.class);

    /**
     * Период проверки подключения для кэша.
     */
    private final long timeout;

    /**
     *
     * @param symbolPriceService - Сервис для получения котировок
     * @param time - время запроса
     */
    public CachedSymbolPriceService(final SymbolPriceService symbolPriceService,
                                    final long time) {
        delegate = symbolPriceService;
        timeout = time;
    }

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого
     * данные будут обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName - строка котировки
     * @return Symbol
     */
    @Override
    public Symbol getPrice(final String symbolName) {
        logger.info("Старт метода getPrice() в кэшированном сервисе.");
        synchronized (symbolName) {
            if (!cache.containsKey(symbolName) || Instant.now()
                    .minus(timeout, ChronoUnit.SECONDS)
                    .isAfter(cache.get(symbolName).getTimeStamp())) {
                cache.put(symbolName, delegate.getPrice(symbolName));
            }
        }
        return cache.get(symbolName);
    }
}
