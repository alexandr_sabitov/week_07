package ru.edu.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.SymbolPriceService;


public class WatcherJob implements Runnable {

    /**
     * Котировка.
     */
    private final String symbolToWatch;
    /**
     * Логгер.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(WatcherJob.class);

    /**
     * SymbolPriceService.
     */
    private final SymbolPriceService service;

    /**
     * Предыдущий запрос.
     */
    private Symbol lastData = null;

    /**
     * Конструктор.
     * @param sym
     * @param serv
     */
    public WatcherJob(final String sym, final SymbolPriceService serv) {
        symbolToWatch = sym;
        service = serv;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        Symbol symbol = service.getPrice(symbolToWatch);
        LOGGER.info("Получен ответ: {}, ранее {}", symbol, lastData);
        lastData = symbol;


    }
}
