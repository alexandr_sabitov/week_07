package ru.edu.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.SymbolPriceService;



public class Watcher implements Runnable {


    /**
     * Логгер.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Watcher.class);
    /**
     * Sleep.
     */
    public static final long MILLIS = 2_500L;
    /**
     * Котировка.
     */
    private final String symbolToWatch;
    /**
     * SymbolPriceService.
     */
    private final SymbolPriceService service;
    /**
     * Конструктор.
     * @param sym
     * @param serv
     */
    public Watcher(final String sym, final SymbolPriceService serv) {
        symbolToWatch = sym;
        service = serv;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        Symbol lastData = null;
        while (true) {
            Symbol symbol = service.getPrice(symbolToWatch);
            LOGGER.info("Получен ответ: {}", symbol);

            try {
                Thread.sleep(MILLIS);
            } catch (InterruptedException e) {

            }


        }

    }
}
