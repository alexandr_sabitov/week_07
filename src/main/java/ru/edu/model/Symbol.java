package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Интерфейс информации о курсе обмена.
 */
public interface Symbol {

    /**
     * Получение  имени котировки.
     * @return symbol
     */
    String getSymbol();

    /**
     * Получение  цены котировки.
     * @return price
     */
    BigDecimal getPrice();

    /**
     * Время получения данных.
     *
     * @return time
     */
    Instant getTimeStamp();
}
